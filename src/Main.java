import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] input = new String[4];
        for (int i = 0; i < 4; i++)
            input[i] = sc.next();
        if (!input[0].equals("java") || !input[1].equals("findregex")) {
            throw new IllegalArgumentException("Invalid command");
        }
        String folderPath = input[2].substring(1, input[2].length() - 1);
        String regex = input[3].substring(1, input[3].length() - 1);
        RegexSearcher rs = new RegexSearcher();
        HashMap<String, ArrayList<String>> listOfFiles = rs.listOfFilesWithLinesMatchingRegex(
                folderPath, regex);
        for (Map.Entry<String, ArrayList<String>> entry : listOfFiles.entrySet()) {
            System.out.println("Name of file: " + entry.getKey() + "\nLines:");
            ArrayList<String> lines = entry.getValue();
            for (int i = 0; i < lines.size(); i++) {
                System.out.println(lines.get(i));
            }
            System.out.println();
        }
    }

}
