import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegexSearcher {

    public RegexSearcher() {}

    public HashMap<String, ArrayList<String>> listOfFilesWithLinesMatchingRegex(String folderPath, String regex) {
        File folder = new File(folderPath);
        ArrayList<File> files = new ArrayList<>();
        listFilesForFolder(folder, files);
        HashMap<String, ArrayList<String>> listOfFiles = new HashMap<>();
        for (File file : files) {
            ArrayList<String> lines = new ArrayList<>();
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    Pattern pattern = Pattern.compile(regex);
                    Matcher matcher = pattern.matcher(line);
                    if (matcher.find()) {
                        lines.add(line);
                    }
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (lines.size() > 0) {
                listOfFiles.put(file.getName(), lines);
            }
        }
        return listOfFiles;
    }

    private void listFilesForFolder(File folder, ArrayList<File> files) {
        for (File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry, files);
            } else {
                files.add(new File(folder.toString() + "\\" + fileEntry.getName()));
            }
        }
    }

}
